import src.config as cfg
import os
from src.dataSetCreation.dataSetCreation import generate_mask
import src.dataSetCreation.datasetLists as dl
from src.ml.train import train_unet
from src.analysis.analyze import get_full_analysis, summarize_patients
from src.util.model_manager import ModelManager
from src.analysis.accuracy import calculate_accuracies

model_manager = ModelManager('../models')

if __name__ == '__main__':
    '''Standard workflow'''

    cohorts = [cfg.ac_cohort, cfg.tcga_cohort, cfg.du_cohort, cfg.emc_cohort]
    model_dir = os.path.abspath('../models/')

    for cohort in cohorts:
        # Generate the lists for training/validation/testing for each cohort
        dl.make_list(cohort)
        dl.make_train_val(cohort, 0.2)
        print(f'Made list for {cohort.name}')

        # Generate a mask form the labelled tiles (they are not in the right format when they come directly from qupath)
        # to speed it up use the c++ program for it
        generate_mask(cohort.lbld_tiles_path)
        generate_mask(cohort.lbld_tiles_path)
        generate_mask(cohort.lbld_tiles_path)
        # Once lists have been created, start training, train multiple models on each cohort
        for run in range(10):
            model_path = os.path.join(model_dir, f'{cohort.name}bs={cfg.batch_size}-run{run}-model.h5')
            train_unet(cohort, model_path)

    best_model = model_manager.get_best_model(cfg.ac_cohort.name)
    # make_results(cohort, os.path.join(model_dir, best_emc_model[0]), 10)
    print("Best model:", best_model[0], ", acc: ", best_model[1][0], " loss: ", best_model[1][1])

    # analyze with trained model, modify for other cohort if you want all
    get_full_analysis(['L:\\PDAC-TSR-Xiuxiang\\AC\\Tiles\\Normaliyation'],
                        f'../models/{best_model[0]}',
                        'L:\\PDAC-TSR-Xiuxiang\\AC\\results')
    summarize_patients('L:\\PDAC-TSR-Xiuxiang\\AC\\results',
                       'L:\\PDAC-TSR-Xiuxiang\\AC\\AC6_patient_percentages.csv')
    calculate_accuracies(cohorts, 'data\\accuracies_final.xlsx')
