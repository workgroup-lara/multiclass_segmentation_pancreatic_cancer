from sklearn.metrics import classification_report
import numpy as np
import src.util.model_manager as mm
import src.ml.dataProcessing as preprocess
from src.util.cohort import Cohort
import tensorflow as tf
from src.ml.prediction import predict_tile
import pandas as pd
import matplotlib.pyplot as plt

model_manager = mm.ModelManager('../models')


du_cohort = Cohort('DU Cohort',
                   '/data/DU',
                   'M:\\Training\\DU\\Successful',
                   'M:\\Training\\DU\\tiles_training',
                   'M:\\Training\\DU\\masks')
ac_cohort = Cohort('AC Cohort',
                   '/data/AC',
                   'M:\\Training\\AC\\Successful',
                   'M:\\Training\\AC\\tiles_training',
                   'M:\\Training\\AC\\masks')
tcga_cohort = Cohort('TCGA Cohort',
                     '/data/TCGA',
                     'K:\\Images\\TCGA-PAAD-DX\\2.0\\tiles_training_normalization\\Successful',
                     'K:\\Images\\TCGA-PAAD-DX\\2.0\\tiles_training',
                     'M:\\Training\\TCGA\\masks'
                     )
emc_cohort = Cohort('EMC Cohort',
                    '/data/EMC',
                    'L:\\PDAC-TSR-Xiuxiang\\EMC\\Training_tiles\\Normaliyation\\successful',
                    'L:\\PDAC-TSR-Xiuxiang\\EMC\\Training_tiles\\Original',
                    'M:\\Training\\EMC\\masks')

test_cohort = Cohort('Test Cohort',
                     '/data/TEST',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TEST\\tiles',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TEST\\tiles',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TEST\\tiles')

model_path = '../../../models'



def plot_accuracy():
    """Makes a plots for accuracy/loss for all cohorts"""
    # get history file name
    for cohort_name in ['DU Cohort', 'AC Cohort', 'TCGA Cohort', 'EMC Cohort']:
        model = model_manager.get_best_model(cohort_name)[0]
        hist = model_manager.read_model_history(model)
        plot_hist(hist)


def plot_hist(hist: dict):
    """Plots the history for a given history of a trained model"""
    plt.plot(hist['val_loss'], label='loss', color='red')
    plt.plot(hist['val_categorical_accuracy'], label='categorical accuracy', color='blue')
    plt.plot(hist['loss'], label='loss (training)', color='orange')
    plt.plot(hist['categorical_accuracy'], label='categorical accuracy (training)', color='lightblue')
    plt.legend()
    plt.xlabel('Itteration')
    plt.ylabel('Accuracy/Loss')
    plt.show()


if __name__ == '__main__':
    plot_accuracy()
