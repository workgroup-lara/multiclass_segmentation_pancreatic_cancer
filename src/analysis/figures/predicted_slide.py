import os
import cv2
import tensorflow as tf
from tqdm import tqdm
from src.dataSetCreation.tiling import image_from_tiles, image_mask_predicted_from_tiles, image_predicted_from_tiles
from src.ml.prediction import predict_tile
from src.util.model_manager import ModelManager


model_manager = ModelManager('../../../models')


def make_predicted_tiles(tiles_path: str, cohort_name: str, out_dir: str):
    """
    Function to make a predicted version of tiles
    :param tiles_path: path to tiles
    :param cohort_name: name of cohort (to load correct model)
    :param out_dir: path to directory in which predicted file is saved
    :return:
    """
    tile_names = [img for img in os.listdir(tiles_path) if img.endswith('.jpg')]
    model_path = os.path.join(model_manager.models_path, model_manager.get_best_model(cohort_name)[0])
    model = tf.keras.models.load_model(model_path)
    for tile_name in tqdm(tile_names):
        tile_path = os.path.join(tiles_path, tile_name)
        predicted_img = predict_tile(tile_path, model, (212, 212))
        cv2.imwrite(os.path.join(out_dir, tile_name), predicted_img)


if __name__ == '__main__':

    patiend_ids = ['12-19820.tif - Series 0', 'H13-3895-E']
    # Example training patient
    pat_id = patiend_ids[1]
    path_to_tiles = f'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\Training\\tiles\\{pat_id}\\'
    path_to_predicted_tiles = f'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\Training\\predictions\\{pat_id}\\'
    path_to_masks = f'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\Training\\masks\\{pat_id}\\'
    path_out = f'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\Training'
    # Create dir with all tiles predicted

    if len(os.listdir(path_to_predicted_tiles)) != (len(os.listdir(path_to_tiles))/2):
        make_predicted_tiles(path_to_tiles, 'EMC Cohort', path_to_predicted_tiles)
    image_mask_predicted_from_tiles(
        path_to_tiles,
        path_to_predicted_tiles,
        path_to_masks,
        path_out,
        (212, 212),
        (396,396),
        '.jpg',
        '-mask.png',
        pat_id,
        overlap_tile=396-212
    )
    #image_from_tiles(path_to_masks, os.path.join(path_out, f'{pat_id}_mask.png'), (212, 212), '-mask.png', pat_id)
    #image_from_tiles(path_to_tiles, os.path.join(path_out, f'{pat_id}_original.png'), (396, 396), '.jpg', pat_id, tile_dims=tile_dims, overlap=396-212)
    #image_from_tiles(path_to_predicted_tiles, os.path.join(path_out, f'{pat_id}_predicted.png'), (212, 212), '.jpg', pat_id, tile_dims=tile_dims)



    exit()


    # Example not-training patient
    pat_id = '10-22841.tif - Series 0'
    ### Path to not training tiles
    path_tiles = f'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\NotTraining\\tiles\\{pat_id}'
    path_predicted = f'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\NotTraining\\predictions\\{pat_id}'
    path_out = f'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\NotTraining'

    # Create dir with all tiles predicted
    if os.listdir(path_predicted) != os.listdir(path_tiles):
        make_predicted_tiles(path_tiles, 'TCGA Cohort', path_predicted)
    # worked
    image_predicted_from_tiles(path_tiles, path_predicted, path_out, (212, 212), (396, 396), '.jpg', pat_id, overlap_tile=396-212)
    #image_from_tiles(path_tiles, os.path.join(path_out, f'{pat_id}_original.png'), (396, 396), '.jpg', pat_id, overlap=396-212)
    #image_from_tiles(path_predicted, os.path.join(path_out, f'{pat_id}_predicted.png'), (212,212), '.jpg', pat_id)
    """image_and_mask_from_tiles(path_to_predicted_tiles,
                              path_to_masks,
                              'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig1\\',
                              (212,212),
                              '.jpg',
                              '-mask.png')"""
