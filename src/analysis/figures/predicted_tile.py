import os

import cv2
import tensorflow as tf
from src.dataSetCreation.tiling import image_from_tiles, image_and_mask_from_tiles
from src.ml.prediction import predict_tile
from src.util.model_manager import ModelManager
import numpy as np
import shutil


model_manager = ModelManager('../../../models')


# workflow
# 1. copy patient to new dir
# 2. run function to generate predictions to new folder
# Run function to join original image and predicted image


def copy_masks(tiles: list, masks_path, dest_path: str):
    for tile in tiles:
        tile_name = tile[:tile.rfind('.jpg')] + '-mask.png'
        shutil.copyfile(os.path.join(masks_path, tile_name), os.path.join(dest_path, tile_name))

# Tile (colored), tile (colored), prediction (colored)
def make_tile_figure(tile_path: str, mask_path: str, predicted_path: str, out_dir: str, models_path: str):
    tile = cv2.imread(tile_path)
    mask = cv2.imread(mask_path)
    model_path = os.path.join(models_path, get_best_model('AC Cohort', models_path)[0])
    model = tf.keras.models.load_model(model_path)
    predicted_tile = predict_tile(tile_path, model, (396, 396))

    cv2.imwrite(predicted_path, predicted_tile)
    predicted_tile = cv2.imread(predicted_path)
    # Replace classes by values
    predicted_tile[np.where((predicted_tile == [0, 0, 0]).all(axis=2))] = [0, 255, 0]
    predicted_tile[np.where((predicted_tile == [1, 1, 1]).all(axis=2))] = [255, 0, 0]
    predicted_tile[np.where((predicted_tile == [2, 2, 2]).all(axis=2))] = [0, 0, 255]

    mask[np.where((mask == [0, 0, 0]).all(axis=2))] = [0, 255, 0]
    mask[np.where((mask == [1, 1, 1]).all(axis=2))] = [255, 0, 0]
    mask[np.where((mask == [2, 2, 2]).all(axis=2))] = [0, 0, 255]

    cv2.imwrite(os.path.join(out_dir, "example_predicted_tile.png"), predicted_tile)
    cv2.imwrite(os.path.join(out_dir, "example_tile.png"), tile)
    cv2.imwrite(os.path.join(out_dir, "example_mask.png"), mask)


if __name__ == '__main__':
    path_to_tiles = 'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig2\\tiles'
    path_to_tile = 'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig2\\tiles\\10-18836.tif - Series 0 [x=20988,y=96884,w=396,h=396].jpg'
    path_to_mask = 'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig2\\tiles\\10-18836.tif - Series 0 [x=20988,y=96884,w=396,h=396]-labelled.png'
    path_to_predicted = 'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig2\\predicted\\10-18836.tif - Series 0 [x=20988,y=96884,w=396,h=396].png'
    outdir = 'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig2'
    copy_path = 'C:\\Users\\Working_Group_Lara\\Documents\\unet\\Fig2\\masks'
    models_path = '../../../models'
    if os.listdir(copy_path) == []:
        copy_masks([tile for tile in os.listdir(path_to_tiles) if tile.endswith('.jpg')],
                    'L:\\PDAC-TSR-Xiuxiang\\AC\\masks\\10-18836.tif - Series 0',
                    copy_path)
    make_tile_figure(path_to_tile, path_to_mask, path_to_predicted, outdir, models_path)

