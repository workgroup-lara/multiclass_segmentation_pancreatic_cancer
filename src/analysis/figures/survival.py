import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import statistics
from sklearn.impute import SimpleImputer
from lifelines import KaplanMeierFitter, CoxPHFitter
from lifelines.statistics import logrank_test
from scipy import stats

df_patients = pd.read_csv('../../../data/DU/DUS_patient_percentages.csv', sep=";")
df_clini_du = pd.read_excel('../../data/DU/DU-PDAC-STROMA_CLINI(1).xlsx')


def group_median(df_patient_data) -> list:
    df_patient_data["Immune Cells - in percent"] = df_patient_data["Immune Cells - in percent"].astype(float)
    median = df_patient_data["Immune Cells - in percent"].median()
    return list(df_patient_data["Immune Cells - in percent"].apply(lambda x: 0 if x <= median else 1))


def preprocess(df_clini: pd.DataFrame, df_patient_data: pd.DataFrame):
    df_patient_data["Immune Cells - in percent"] = df_patient_data["Immune Cells - in percent"].astype(float)
    df_patient_data = df_patient_data[df_patient_data['Pat-id'].isin(df_clini["FILENAME"])]
    df_clini = df_clini[df_clini["FILENAME"].isin(df_patient_data["Pat-id"])]
    # median = df_patient_data["Immune Cells - in percent"].median()
    # list(df_patient_data["Immune Cells - in percent"].apply(lambda x: 0 if x <= median else 1))
    df_clini["OS"] = df_clini["OS"].apply(lambda x: True if x == 1 else False)
    df_clini["OS- Time"] = df_clini["OS- Time"].astype(float)
    df_clini["Group"] = group_median(df_patient_data)
    df_clini.rename(columns={"OS- Time": "survival"})
    return df_clini


df_patients = preprocess(df_clini_du, df_patients)

kmf = KaplanMeierFitter()
X = df_patients['survival']
Y = df_patients['dead']
kmf.fit(X, event_observed=Y)
kmf.plot()
plt.title("Kaplan Meier estimates")
plt.xlabel("Month after heart attack")
plt.ylabel("Survival")
plt.show()
