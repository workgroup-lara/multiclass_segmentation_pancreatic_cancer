import numpy as np
import src.util.model_manager as mm
import src.ml.dataProcessing as preprocess
from src.ml.prediction import predict_tile
import pandas as pd
from src.config import du_cohort, tcga_cohort, ac_cohort, emc_cohort, models_path
from sklearn.metrics import multilabel_confusion_matrix
from sklearn.metrics import jaccard_score

model_manager = mm.ModelManager(models_path)


def print_table(table_array: np.array):
    print(f"|    | S | T | I |")
    for index, row in enumerate(table_array):
        row_name: str = ''
        if index == 0:
            row_name = 'S'
        if index == 1:
            row_name = 'T'
        if index == 2:
            row_name = 'I'
        print(f"| {row_name} | {row[0]} | {row[1]} | {row[2]}")


def calculate_accuracies(cohorts: list, out_path: str):
    """
    Function to calculate Intersection over Union, Dice coefficient, Accuracy, Error rate,
    Specificity and Sensitivity for all cohorts. Output is saved to a file"""

    df_iou = pd.DataFrame()
    for cohort in cohorts:
        (_, _), (_, _), (test_x, test_y) = preprocess.load_data(cohort.trainval_path,
                                                                cohort.tiles_path,
                                                                cohort.mask_path)
        if cohort.name == 'Test Cohort':
            best_model = model_manager.get_best_model('DU Cohort')
        else:
            best_model = model_manager.get_best_model(cohort.name)
        best_model_name = best_model[0]
        model = model_manager.get_model(best_model_name)

        # Predict tile and load corresponding mask
        test_predicted = list(map(lambda x: predict_tile(x, model, (396, 396)), test_x))
        test_y = list(map(lambda x: preprocess.read_mask(x), test_y))
        # Flatten arrays for comparison
        test_predicted = list(map(lambda x: np.ndarray.flatten(x), test_predicted))
        test_y = list(map(lambda x: np.ndarray.flatten(x), test_y))
        test_y = np.concatenate(test_y).ravel()
        test_predicted = np.concatenate(test_predicted).ravel()
        print(test_y[1:20])
        print(test_predicted[1:20])

        dices_per_class: list = []
        iou_per_class: list = []
        accuracy_per_class: list = []
        total_error_per_class: list = []
        specificity_per_class: list = []
        sensitivity_per_class: list = []

        # Structure of confusion matrix
        # [[tn, fp],
        #  [fn, tp]]
        conf_matrix = multilabel_confusion_matrix(test_y, test_predicted, labels=[0, 1, 2])
        # Jaccard Score (IOU Score
        jac = jaccard_score(test_y, test_predicted, labels=[0, 1, 2], average="macro")
        for num_class in range(3):
            # accuracy = (tp+tn)/(tp+tn+fn+fp)
            # IoUs = tp/(tp+fp+fn)
            # Dice coefficient = 2*tp/(2*tp + fp + fn)
            tp_tn_fp_fn = conf_matrix[num_class][1][1] + conf_matrix[num_class][0][1] + conf_matrix[num_class][1][0] \
                          + conf_matrix[num_class][0][0]
            iou_per_class.append(
                conf_matrix[num_class][1][1] / (np.sum(conf_matrix[num_class][1]) + conf_matrix[num_class][0][1]))
            dices_per_class.append(
                2 * conf_matrix[num_class][1][1] / (2 * conf_matrix[num_class][1][1] + conf_matrix[num_class][1][0]
                                                    + conf_matrix[num_class][0][1]))
            accuracy_per_class.append((conf_matrix[num_class][1][1] + conf_matrix[num_class][0][0]) / tp_tn_fp_fn)
            total_error_per_class.append((conf_matrix[num_class][0][1] + conf_matrix[num_class][1][0]) / tp_tn_fp_fn)
            specificity_per_class.append(
                conf_matrix[num_class][0][0] / (conf_matrix[num_class][0][0] + conf_matrix[num_class][0][1]))
            sensitivity_per_class.append(conf_matrix[num_class][1][1] /
                                         (conf_matrix[num_class][1][1] + conf_matrix[num_class][1][0]))
            print(conf_matrix[num_class])
        print(f"Acc for {cohort.name} is {np.sum(accuracy_per_class) / 3}")
        print(f"IoU for {cohort.name} is sklearn: {np.sum(iou_per_class) / 3} (cm), sklearn: {jac}")
        print(f"Dice for {cohort.name} is: Dices:{np.sum(dices_per_class) / 3}")

        df_iou = df_iou.append([{"Cohort": f"{cohort.name}",
                                 'Total acc': np.sum(accuracy_per_class) / 3,
                                 'Class 0 acc (Stroma)': accuracy_per_class[0],
                                 'Class 1 acc (Tumor)': accuracy_per_class[1],
                                 'Class 2 acc (immune cells)': accuracy_per_class[2],
                                 'Mean Intersection over union (sklearn)': jac,
                                 'Mean IOU rates': np.sum(iou_per_class) / 3,
                                 'Mean IOU rates Stroma': iou_per_class[0],
                                 'Mean IOU rates Tumor': iou_per_class[1],
                                 'Mean IOU rates Immune': iou_per_class[2],
                                 'Mean dice coefficient Total': np.sum(dices_per_class) / 3,
                                 'Mean dice coefficient Stroma': dices_per_class[0],
                                 'Mean dice coefficient Tumor': dices_per_class[1],
                                 'Mean dice coefficient Immune': dices_per_class[2],
                                 'Total mean error': np.sum(total_error_per_class) / 3,
                                 'Specificity Total': np.sum(specificity_per_class) / 3,
                                 'Specificity Stroma': specificity_per_class[0],
                                 'Specificity Tumor': specificity_per_class[1],
                                 'Specificity Immune': specificity_per_class[2],
                                 'Sensitivity Total': np.sum(sensitivity_per_class) / 3,
                                 'Sensitivity Stroma': sensitivity_per_class[0],
                                 'Sensitivity Tumor': sensitivity_per_class[1],
                                 'Sensitivity Immune': sensitivity_per_class[2],
                                 }])
    df_iou.to_excel(out_path)


if __name__ == '__main__':
    used_cohorts = [ac_cohort, tcga_cohort, du_cohort, emc_cohort]
    calculate_accuracies(used_cohorts, 'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\'
                                       'Intersection_over_union_final.xlsx')
