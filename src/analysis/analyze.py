"""Module to analyze a given dataset of tiles with a given neural network"""
import os
import tensorflow as tf
import numpy as np
import pandas as pd
from tqdm import tqdm
from src.ml.prediction import predict_tile
from src.config import shape


def get_patient_ids(slide_list):
    """
    Extracts all unique patient names form a list of slides
    :param slide_list: list of slide names
    :return: list of unique patient ids
    """
    pat_ids = set()
    for slide_name in [slide for slide in slide_list if not (slide.endswith('.txt')
                                                             or slide.endswith('.bat')
                                                             or slide.endswith('.TXT'))]:
        start_index = slide_name.find('-')
        pat_id_start = slide_name.rfind(' ', 0, start_index)
        pat_id_end = slide_name.find(' ', start_index)
        pat_ids.add(slide_name[pat_id_start + 1:pat_id_end])
    return sorted(list(pat_ids))


def get_tile_class_dist(tile_path, model_path) -> (list, list):
    """
    Gets a class distribution for a tile and a neural network model. Returns a list with the unique values
    (classes) and a second list with the corresponding counts
    :param tile_path:
    :param model_path:
    :return:
    """
    model = tf.keras.models.load_model(model_path)
    p = predict_tile(tile_path, model, shape)
    return np.unique(p, return_counts=True)


def get_patient_class_dist(pat_id, tiles_path, model, output_path) -> None:
    """
    Gets the class distribution of each tile of a patient in total counts and percentage
    :param pat_id: ID of patient analyzed
    :param tiles_path: path to the tiles of the patient
    :param model_path: path to neural network model
    :param output_path: path to the file in which the counts for each tile will be written
    :return:
    """
    # Get all tiles from patient
    pat_tiles = [tile for tile in os.listdir(tiles_path) if tile.endswith('.jpg') and pat_id in tile]
    # Initialize dataframe
    df_tile_dist = pd.DataFrame({'Tile Name': [],
                                 'Stroma - Total pixels': [],
                                 'Tumor - Total pixels': [],
                                 'Immune Cells - Total pixels': [],
                                 'Stroma - in percent': [],
                                 'Tumor - in percent': [],
                                 'Immune Cells - in percent': []
                                 })

    for tile in pat_tiles:
        p = predict_tile(os.path.join(tiles_path, tile), model, shape)
        class_value, frequency = np.unique(p, return_counts=True)
        # Add measurements to dataframe
        new_row = [tile, 0, 0, 0, 0, 0, 0]
        # Set found values in new row
        total_pixels = sum(frequency)
        for index, value in enumerate(class_value):
            # Check if values was legal
            if not 0 <= value <= 2:
                raise Exception(f'Critical error, a prediction value out of scope was found ({value}) on {tile}!')
            new_row[value + 1] = frequency[index]
            new_row[value + 1 + 3] = frequency[index] / total_pixels

        df_tile_dist.loc[len(df_tile_dist.index)] = new_row

    # Save dataframe
    df_tile_dist.to_csv(os.path.join(output_path, pat_id + '.csv'))


def get_full_analysis(cohort_paths: list, model_path, output_path):
    """

    :param cohort_paths: List of paths to directory which contains the patients folder
    :param model_path: Path to used model
    :param output_path: path to file with results for patients
    :return:
    """
    # Dict with pat id as key and path as value
    pat_ids_paths = {}
    for cohort_path in cohort_paths:
        for pat_id in os.listdir(cohort_path):
            pat_ids_paths[pat_id] = os.path.join(cohort_path, pat_id)
    # Load model
    model = tf.keras.models.load_model(model_path)
    for pat_id in tqdm(pat_ids_paths.keys()):
        # Skipp already existing ones
        if os.path.isfile(os.path.join(output_path, pat_id + '.csv')):
           continue
        get_patient_class_dist(pat_id, pat_ids_paths[pat_id], model, output_path)


def summarize_patients(dir_path: str, output_path: str):
    """
    Method to combine the patient results to one csv with the average percentages of all tiles for each patient
    :param dir_path: path the the files of the patients
    :param output_path: filepath for summary csv file
    """
    header = ['Pat-id', "Tumor - in percent", "Stroma - in percent", "Immune Cells - in percent"]
    df_patients = pd.DataFrame(columns=header)

    for file in tqdm(os.listdir(dir_path)):
        pat_id = get_patient_ids([file])[0]
        file_path = os.path.join(dir_path, file)
        df_patient = pd.read_csv(file_path, sep=",")

        df_patients = df_patients.append({'Pat-id': pat_id,
                                          "Tumor - in percent": df_patient['Tumor - in percent'].mean(),
                                          "Stroma - in percent": df_patient['Stroma - in percent'].mean(),
                                          "Immune Cells - in percent": df_patient['Immune Cells - in percent'].mean()},
                                         ignore_index=True)

    df_patients.to_csv(output_path, sep=";", header=True)




def convert_from_old(old_path:str , new_path: str) -> None:
    df_old = pd.read_csv(old_path, sep=",")
    if not 'Background - Total pixels' in df_old.columns.tolist():
        raise ValueError("Dataframe is not and old dataframe")

    df_old = df_old.rename(columns={'Background - Total pixels': 'Stroma - Total pixels',
                   'Stroma - Total pixels': 'Tumor - Total pixels',
                   'Tumor - Total pixels': 'Immune Cells - Total pixels',
                   'Immune Cells - Total pixels': 'dropped1',
                   'Background - in percent': 'Stroma - in percent',
                   'Stroma - in percent': 'Tumor - in percent',
                   'Tumor - in percent': 'Immune Cells - in percent',
                   'Immune Cells - in percent': 'dropped2'})
    df_new = df_old.drop(columns=['dropped1', 'dropped2', 'Unnamed: 0'])
    df_new.to_csv(new_path, sep=";", index=False)


if __name__ == '__main__':
    #get_patient_class_dist('TCGA-2J-AAB1-01Z-00-DX1.F3B4818F-9C3B-4C66-8241-0570B2873EC9', '../../data/TCGA/tiles/',
    #                       '../../models/model_TCGA_ot.h5', '../../data/TCGA')
    #get_full_analysis('K:\Classifier_tiles\DU\Success', '../../models/model.h5', 'X:\CF_analysis')
    convert_from_old('M:\\Classifier_tiles\\AC\\results\\10-17856.tif - Series 0', 'M:\\Classifier_tiles\\AC\\results\\10-17856.tif - Series 0_new')
