"""Interface to work with models"""
import os
import pickle
from dataclasses import dataclass
import tensorflow as tf


@dataclass
class ModelManager:
    models_path: str

    def get_model(self, model_name) -> tf.keras.models.Model:
        return tf.keras.models.load_model(os.path.join(self.models_path, model_name))

    def get_model_names(self) -> list:
        return [path for path in os.listdir(self.models_path) if path.endswith('.h5')]

    def get_cohort_model_names(self, cohort_name)-> list:
        return [path for path in self.get_model_names() if cohort_name in path]

    def read_model_history(self, model_name: str) -> dict:
        """
        Reads and returns the history of a given model
        :param model_name: name of the model including the ending
        """
        hist_name = model_name[:model_name.rfind('.')]
        with open(os.path.join(self.models_path, hist_name), 'rb') as f_hist:
            history = pickle.load(f_hist)
        return history

    def get_model_acc(self) -> dict:
        """
        Returns a dict of models, key is the model name, value is its accuracy (highest observed) and loss (of the train
        set, so its val_loss and val_acc. Also lowest loss). Example: "AC Cohort": (0.76, 0.23)
        """
        models = self.get_model_names()
        model_metrics = dict()
        for model in models:
            hist = self.read_model_history(model)
            model_metrics.update({model: (max(hist['val_categorical_accuracy']), min(hist['val_loss']))})
        return model_metrics

    def get_best_model(self, cohort_name: str) -> (str, tf.keras.models.Model):
        model_metrics = self.get_model_acc()
        best_model = tuple()
        for model in model_metrics.keys():
            if cohort_name not in model:
                continue
            if not best_model or model_metrics[model][0] > best_model[1]:
                best_model = (model, model_metrics[model][0])
        return best_model