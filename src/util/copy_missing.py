from src.config import tcga_cohort
from src.ml.dataProcessing import load_data, tf_dataset
import os
import shutil


def copy_tiles(path_new_tiles: str, dest: str)-> None:
    _, _, (needed_test_tiles, _) = load_data(tcga_cohort.trainval_path, tcga_cohort.tiles_path, tcga_cohort.mask_path)
    needed_test_tiles = [test_tile[test_tile.rfind('\\')+1:] for test_tile in needed_test_tiles]
    available_tiles = set()
    for dir in os.listdir(tcga_cohort.tiles_path):
        for tile in os.listdir(os.path.join(tcga_cohort.tiles_path, dir)):
            available_tiles.add(tile)
    missing_tiles = (set(needed_test_tiles) - set(needed_test_tiles).intersection(available_tiles))
    for missing_tile in missing_tiles:
        missing_tile = os.path.join(missing_tile[:missing_tile.find(" ")], missing_tile)
        patient_dir = os.path.join(dest, missing_tile[:missing_tile.find("\\")])
        if not os.path.isdir(patient_dir):
           os.mkdir(patient_dir)
        shutil.copy2(os.path.join(path_new_tiles, missing_tile), os.path.join(dest, missing_tile))





if __name__ == '__main__':
    copy_tiles('L:\\PDAC-TSR-Xiuxiang\\Training\\TCGA\\TCGA','L:\\PDAC-TSR-Xiuxiang\\Training\\TCGA\\ToNormalize')