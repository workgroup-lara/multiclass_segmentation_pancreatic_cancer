"""Module to define a cohort class"""
import os


def path_validator(path: str, error_msg: str):
    """
    Checks if path actually exists, if not raises ValueError exception. If a path exists it will be converted into
    an absolut path.
    :param path: path to validate
    :param error_msg: name of path that will be displayed
    :return:
    """
    if not os.path.isdir(path):
        raise ValueError(f"New '{error_msg}' ({path}) path is not an actual directory, please create it first")
        #print(f"Warning, directory {path} does not exist")
    return os.path.abspath(path)


class Cohort:

    def __init__(self, name, trainval_path: str, tiles_path: str, labelled_tiles_path='', mask_path=''):
        self._name = name
        self.trainval_path = trainval_path
        self.tiles_path = tiles_path
        if not labelled_tiles_path:
            self.lbld_tiles_path = tiles_path
        else:
            self.lbld_tiles_path = labelled_tiles_path
        if not mask_path:
            self.mask_path = tiles_path
        else:
            self.mask_path = mask_path


    @property
    def name(self):
        return self._name

    @property
    def trainval_path(self):
        return self._trainval_path

    @property
    def tiles_path(self):
        return self._tiles_path

    @property
    def lbld_tiles_path(self):
        return self._lbld_tiles_path

    @property
    def mask_path(self):
        return self._mask_path

    @name.setter
    def name(self, new_name: str):
        if new_name == self._name:
            raise ValueError("Error name did not change")
        self._name = new_name

    @trainval_path.setter
    def trainval_path(self, new_path: str):
        new_path = path_validator(new_path, 'train/validation')
        self._trainval_path = new_path

    @tiles_path.setter
    def tiles_path(self, new_path: str):
        new_path = path_validator(new_path, 'tiles')
        self._tiles_path = new_path

    @lbld_tiles_path.setter
    def lbld_tiles_path(self, new_path: str):
        new_path = path_validator(new_path, 'labelled')
        self._lbld_tiles_path = new_path

    @mask_path.setter
    def mask_path(self, new_path: str):
        new_path = path_validator(new_path, 'mask')
        self._mask_path = new_path

    def __str__(self):
        return f"Cohort: {self.name}, trainval path: {self.trainval_path}, tiles path: {self.tiles_path}, labelled tiles path:" \
               f" {self.lbld_tiles_path}, masks path:{self.mask_path} "

    def __repr__(self):
        return f"Cohort({self.name}, {self.trainval_path}, {self.tiles_path}, {self.lbld_tiles_path}, {self.mask_path}"
