import cv2
import numpy as np
import os

def reconvert_mask(mask_path: str):
    mask = cv2.imread(mask_path)
    mask[np.where((mask == [0, 0, 0]).all(axis=2))] = [0, 255, 0]
    mask[np.where((mask == [1, 1, 1]).all(axis=2))] = [255, 0, 0]
    mask[np.where((mask == [2, 2, 2]).all(axis=2))] = [0, 0, 255]

    cv2.imshow("test",mask)
    cv2.waitKey(0)

    # closing all open windows
    cv2.destroyAllWindows()
    #cv2.imwrite(os.path.join(out_dir, "example_mask.png"), mask)


if __name__ == '__main__':
    reconvert_mask('L:\\PDAC-TSR-Xiuxiang\\TCGA\\masks\\TCGA-2J-AAB1-01Z-00-DX1.F3B4818F-9C3B-4C66-8241-0570B2873EC9\\TCGA-2J-AAB1-01Z-00-DX1.F3B4818F-9C3B-4C66-8241-0570B2873EC9 [x=37948,y=59148,w=396,h=396]-mask.png')