import os
import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from src.ml.dataProcessing import load_data, tf_dataset
from src.ml.NeuralNetworks.unet import build_unet
from src.config import *
import pickle


def train_unet(cohort: Cohort, model_path: str):
    """
    Function to train a unet with the parameters set in the config and with the given cohort
    :param cohort: Cohort being used
    :param model_path: path where the trained model will be saved to
    :return:
    """

    """ Seeding """
    np.random.seed(42)
    tf.random.set_seed(42)

    """ Dataset """
    (train_x, train_y), (valid_x, valid_y), (test_x, test_y) = load_data(cohort.trainval_path, cohort.tiles_path,
                                                                         cohort.mask_path)
    print(f"Dataset: Train: {len(train_x)} - Valid: {len(valid_x)} - Test: {len(test_x)}")

    """ Model """
    model = build_unet(shape, numeric_classes)
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(), optimizer='adam',
                  metrics=['categorical_accuracy', 'accuracy'])

    train_dataset = tf_dataset(train_x, train_y, batch=batch_size)
    valid_dataset = tf_dataset(valid_x, valid_y, batch=batch_size)

    train_steps = len(train_x) // batch_size
    valid_steps = len(valid_x) // batch_size

    callbacks = [
        ModelCheckpoint(model_path, verbose=1, save_best_only=True),
        ReduceLROnPlateau(monitor="val_loss", patience=3, factor=0.1, verbose=1, min_lr=lr),
        EarlyStopping(monitor="val_loss", patience=5, verbose=1)
    ]

    tr_data = model.fit(train_dataset,
                        steps_per_epoch=train_steps,
                        validation_data=valid_dataset,
                        validation_steps=valid_steps,
                        epochs=epochs,
                        callbacks=callbacks
                        )
    # Save history of training in case it should be plotted later
    with open(model_path[:model_path.rfind('.')], 'wb') as file_pi:
        pickle.dump(tr_data.history, file_pi)
