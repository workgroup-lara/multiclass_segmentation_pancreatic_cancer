"""Module to predict classes on images"""
import os.path

import tensorflow as tf
import numpy as np
import cv2


def predict_tile(tile_path: str, model: tf.keras.models.Model, shape=()) -> np.array:
    """Takes a tile as an input for the given neural network. Returns the output of the neural network (predicted mask)
    as a numpy object that haas the shape of the output of the neural network. The mask contains the numeric classes
    (each pixel has values in the intervall [1,...,numeric_classes]. Output is not an image to display but can easyly
    converted as one.

    :param tile_path: path to the tile used as input for the neural network
    :param model: neural network model
    :param shape: shape of the input of the neural network
    :return:
    """
    # Read image
    if not os.path.isfile(tile_path):
        raise ValueError(f"tile {tile_path} could not be found")
    x = cv2.imread(tile_path, cv2.IMREAD_COLOR)
    # x = cv2.resize(x, (shape[0], shape[1]))
    x = x / 255.0
    x = x.astype(np.float32)

    # Prediction
    p = model.predict(np.expand_dims(x, axis=0))[0]
    p = np.argmax(p, axis=-1)
    p = np.expand_dims(p, axis=-1)
    # p = p * (255 / numeric_classes)
    p = p.astype(np.int32)
    # p = np.concatenate([p, p, p], axis=2)
    return p
