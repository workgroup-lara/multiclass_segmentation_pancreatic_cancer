from tensorflow.keras.layers import Input, Conv2D, Conv2DTranspose, BatchNormalization, MaxPooling2D, Conv2DTranspose, \
    Concatenate, Dropout
from tensorflow.keras.models import Model
import tensorflow as tf


def conv_block(inputs, filters, pool=True):
    x = Conv2D(filters, (3, 3), activation="relu", kernel_initializer='he_normal', padding="valid")(inputs)
    # x = BatchNormalization()(x)
    x = Dropout(0.10)(x)
    x = Conv2D(filters, (3, 3), activation="relu", kernel_initializer='he_normal', padding="valid")(x)
    # x = BatchNormalization()(x)

    if pool:
        p = MaxPooling2D((2, 2))(x)
        return x, p
    else:
        return x


def crop_concat(x2, x1):
    x1_shape = x1.get_shape()
    x2_shape = x2.get_shape()
    # offsets for the top left corner of the crop
    offsets = [0, (x1_shape[1] - x2_shape[1]) // 2, (x1_shape[2] - x2_shape[2]) // 2, 0]
    size = [-1, x2_shape[1], x2_shape[2], -1]
    x1_crop = tf.slice(x1, begin=offsets, size=size)
    return Concatenate()([x1_crop, x2])


def build_unet(shape, num_classes):
    inputs = Input(shape)

    """ Encoder """
    x1, p1 = conv_block(inputs, 16, pool=True)
    x2, p2 = conv_block(p1, 32, pool=True)
    x3, p3 = conv_block(p2, 64, pool=True)
    x4, p4 = conv_block(p3, 128, pool=True)

    """ Bridge """
    b1 = conv_block(p4, 256, pool=False)

    """ Decoder """
    u1 = Conv2DTranspose(128, (2, 2), strides=(2, 2), kernel_initializer='he_normal', padding='valid')(b1)
    c1 = crop_concat(u1, x4)
    x5 = conv_block(c1, 128, pool=False)

    u2 = Conv2DTranspose(64, (2, 2), strides=(2, 2), kernel_initializer='he_normal', padding='valid')(x5)
    c2 = crop_concat(u2, x3)
    x6 = conv_block(c2, 64, pool=False)

    u3 = Conv2DTranspose(32, (2, 2), strides=(2, 2), kernel_initializer='he_normal', padding='valid')(x6)
    c3 = crop_concat(u3, x2)
    x7 = conv_block(c3, 32, pool=False)

    u4 = Conv2DTranspose(16, (2, 2), strides=(2, 2), kernel_initializer='he_normal', padding='valid')(x7)
    c4 = crop_concat(u4, x1)
    x8 = conv_block(c4, 16, pool=False)

    """ Output layer """
    output = Conv2D(num_classes, (1, 1), padding="same", activation="softmax")(x8)

    return Model(inputs, output)


if __name__ == "__main__":
    model = build_unet((396, 396, 3), 4)
    model.summary()
