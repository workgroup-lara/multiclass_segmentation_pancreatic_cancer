import numpy as np
import tensorflow as tf
from tqdm import tqdm
import cv2
import os
from src.ml.dataProcessing import load_data
from src.config import height, width, output_width, output_height, numeric_classes
from src.util.cohort import Cohort


def make_results(cohort: Cohort, model_path: str, amount: int) -> None:
    """
    Makes the result images of given cohort and given model and places them in the results folder of the given cohort.
    :param cohort: Cohort being used
    :param model_path: path to the model being used
    :param amount: amount of test images generated
    :return:
    """

    """ Seeding """
    np.random.seed(42)
    tf.random.set_seed(42)

    """ Dataset """
    (train_x, train_y), (valid_x, valid_y), (test_x, test_y) = load_data(cohort.trainval_path, cohort.tiles_path, cohort.mask_path)
    print(f"Data set: Train: {len(train_x)} - Valid: {len(valid_x)} - Test: {len(test_x)}")

    model = tf.keras.models.load_model(model_path)
    padding = int((height - output_height) / 2)

    """ Saving the masks """
    for x, y in tqdm(zip(test_x, test_y), total=len(test_x)):
        name = x.split("\\")[-1]

        if ":" in name:
            raise ValueError("Error name likely contains full path, check name in make_results!")
        ## Read image
        x = cv2.imread(x, cv2.IMREAD_COLOR)
        x = cv2.resize(x, (width, height))
        x = x / 255.0
        x = x.astype(np.float32)

        ## Read mask
        y = cv2.imread(y, cv2.IMREAD_GRAYSCALE)
        y = cv2.resize(y, (width, height))
        y = np.expand_dims(y, axis=-1)
        y = y * (255 / numeric_classes)
        y = y.astype(np.int32)
        y = np.concatenate([y, y, y], axis=2)

        ## Prediction
        p = model.predict(np.expand_dims(x, axis=0))[0]
        p = np.argmax(p, axis=-1)
        p = np.expand_dims(p, axis=-1)
        p = p * (255 / numeric_classes)
        p = p.astype(np.int32)
        p = np.concatenate([p, p, p], axis=2)

        x = x * 255.0
        x = x.astype(np.int32)

        h, w, _ = x.shape
        line = np.ones((output_height, 10, 3)) * 255

        x = x[padding:padding + output_height, padding:padding + output_width]
        y = y[padding:padding + output_height, padding:padding + output_width]

        final_image = np.concatenate([x, line, y, line, p], axis=1)
        out_path = os.path.join(cohort.trainval_path, f'results/{name}')
        cv2.imwrite(out_path, final_image)
