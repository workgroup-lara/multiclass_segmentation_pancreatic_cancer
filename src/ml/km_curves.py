import pandas as pd
import matplotlib.pyplot as plt
from lifelines import KaplanMeierFitter
from lifelines.plotting import add_at_risk_counts
from matplotlib.ticker import FuncFormatter
from lifelines.statistics import logrank_test
import matplotlib.pyplot as plt


def simple_kaplan_meier_curve(df_clini, group_column, group_name, cohort_name):

    fig = plt.figure()
    ax = plt.subplot(111)
    df_high_low = df_clini[df_clini[group_column] == 0]
    df_intermediate = df_clini[df_clini[group_column] == 1]

    high_low = KaplanMeierFitter()
    high_low.fit(df_high_low['Days Postop Survival'], event_observed=df_high_low['OS'], label=f"{group_name} high-low")
    high_low.plot_survival_function(ci_show=False, color='red', ax=ax)

    intermediate = KaplanMeierFitter()
    intermediate.fit(df_intermediate['Days Postop Survival'], event_observed=df_intermediate['OS'], label=f"{group_name} intermediate")
    intermediate.plot_survival_function(ci_show=False, color='blue', ax=ax)

    # Set outline to desired color
    """    outline_color = 'yellow'
    for side in ['top', 'bottom', 'right', 'left']:
        ax.spines[side].set_color(outline_color)"""

    # ax.yaxis.set_major_formatter(FuncFormatter('{0:.0%}'.format))# Sets y-axis to percentage values
    results = logrank_test(df_high_low['Days Postop Survival'], df_intermediate['Days Postop Survival'],
                           event_observed_A=df_high_low['OS'], event_observed_B=df_intermediate['OS'])
    # Title and axis labels
    plt.title(f'Kaplan Meier curve for {cohort_name}')
    plt.xlabel('Survival time in months')
    plt.xlim(0, 60)
    plt.ylabel('Survival')
    #plt.tight_layout()
    print(results.p_value)  # 0.7676
    plt.show()


if __name__ == '__main__':
    df_clini = pd.read_csv("../../data/AC-.txt", sep="\t")
    df_clini["Days Postop Survival"] = df_clini["Days Postop Survival"].div(30.5)

    #df_clini['OS'] = ((df_clini['OS'] + 1) % 2)
    simple_kaplan_meier_curve(df_clini, "Group", "SIP", "AC")
    simple_kaplan_meier_curve(df_clini, "groupI", "LIP", "AC")

    df_clini = pd.read_csv("../../data/ECM-.txt", sep="\t")
    df_clini["Days Postop Survival"] = df_clini["Days Postop Survival"].div(30.5)
    simple_kaplan_meier_curve(df_clini, "group", "SIP", "ECM")
    simple_kaplan_meier_curve(df_clini, "groupI", "LIP", "ECM")

    df_clini = pd.read_csv("../../data/DUS-.txt", sep="\t")
    df_clini["Days Postop Survival"] = df_clini["Days Postop Survival"].div(30.5)
    simple_kaplan_meier_curve(df_clini, "group", "SIP", "DUS")
    simple_kaplan_meier_curve(df_clini, "groupI", "LIP", "DUS")

    df_clini = pd.read_csv("../../data/TCGA2-.txt", sep="\t")
    df_clini["Days Postop Survival"] = df_clini["Days Postop Survival"].div(30.5)
    simple_kaplan_meier_curve(df_clini, "group", "SIP", "TCGA")
    simple_kaplan_meier_curve(df_clini, "groupI", "LIP", "TCGA")