"""File containing all the static parameters such as paths"""
from src.util.cohort import Cohort
import os

# If paths change modify them here
du_cohort = Cohort('DU Cohort',
                   'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\DU',
                   'L:\\PDAC-TSR-Xiuxiang\\DUS\\M_copy\\Successful',
                   'L:\\PDAC-TSR-Xiuxiang\\DUS\\M_copy\\tiles_training',
                   'L:\\PDAC-TSR-Xiuxiang\\DUS\\M_copy\\masks')
ac_cohort = Cohort('AC Cohort',
                   'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\AC',
                   'L:\\PDAC-TSR-Xiuxiang\\Training\\AC\\Successful',#'K:\\Training\\AC\\Successful',
                   'L:\\PDAC-TSR-Xiuxiang\\AC\\Training-tiles\\Normalization',#'K:\\Training\\AC\\tiles_training',
                   'L:\\PDAC-TSR-Xiuxiang\\AC\\masks')
tcga_cohort = Cohort('TCGA Cohort',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TCGA',
                     'L:\\PDAC-TSR-Xiuxiang\\Training\\TCGA\\Normalization',
                     'L:\\PDAC-TSR-Xiuxiang\\TCGA\\Training_tiles\\Normaliyation',
                     'L:\\PDAC-TSR-Xiuxiang\\TCGA\\masks'
                     )
emc_cohort = Cohort('EMC Cohort',
                    'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\EMC',
                    'L:\\PDAC-TSR-Xiuxiang\\EMC\\Training_tiles\\Normaliyation\\successful',
                    'L:\\PDAC-TSR-Xiuxiang\\EMC\\Training_tiles\\Original',
                    'L:\\PDAC-TSR-Xiuxiang\\EMC\\masks')

test_cohort = Cohort('Test Cohort',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TEST',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TEST\\tiles',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TEST\\tiles',
                     'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\data\\TEST\\tiles')

models_path = 'C:\\Users\\Working_Group_Lara\\Desktop\\Projects\\Unet_Classifier\\models'

# Hyperparameters for training
numeric_classes = 3
height = 396
width = 396
output_height = 212
output_width = 212
shape = (height, width, 3)
num_classes = numeric_classes
lr = 0.000001
batch_size = 160
epochs = 50

# values to change in case script should be modified
class_values = [0, 1, 2]
classes = {0: 'Stroma',
           1: 'Tumor',
           2: 'Immune Cells'}

tiles_header = ['excluded', 'valid', 'potentiallyBad', 'revision', 'updated']
