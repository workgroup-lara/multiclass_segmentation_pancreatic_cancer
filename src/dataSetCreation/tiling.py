import os

os.environ["OPENCV_IO_MAX_IMAGE_PIXELS"] = pow(2, 40).__str__()
import cv2
import numpy as np
from tqdm import tqdm


def tile_image(image_path, output_dir: str, tile_shape: tuple, overlap: int = 0) -> None:
    """
    Crops an image into smaller images with the size of 'tile_shape'. Smaller Images
    can overlap if needed. Note that this might not work for too large images, in this
    case the 2nd line of the import hast to be adjusted.

    :param image_path: path to image
    :param tile_shape: shape of tiles being created (x,y)
    :param output_dir: path to directory where the tiles will be stored
    :param overlap: overlap that the tiles should have (shared area in between the tiles) for border tiles, the image is
    mirrored at the border.
    :return:
    """
    # Extract image name from path
    img_name = image_path[image_path.rfind('/') + 1:image_path.rfind(".")]

    img = cv2.imread(image_path)
    if img is None:
        print(f"Error loading Image, apparently '{image_path}' does not exists.")
        return
    if overlap % 2 != 0:
        print('Error, overlap must be dividable by 2 as overlap/2 is added around the corners')
        return

    # Calculate actual tile height (without overlap)
    tile_width = tile_shape[0] - overlap
    tile_height = tile_shape[1] - overlap

    # Calculate offset of pixel positions (offset x = offset y)
    offset = int(overlap / 2)
    # Get the total amount of tiles we can get in both directions
    fit_x = img.shape[1] // tile_width
    fit_y = img.shape[0] // tile_height

    # If we have Overlap, add mirrored border to image in order to make tiles at border with same size
    if overlap > 0:
        img = cv2.copyMakeBorder(img, offset, offset, offset, offset, cv2.BORDER_REFLECT)

    for x in tqdm(range(fit_x)):
        for y in range(fit_y):
            x_pos = (x * tile_width) + offset
            y_pos = (y * tile_height) + offset

            # Make tile name similar to how QuPath makes names
            file_name = f"{img_name} [x={x_pos - offset},y={y_pos - offset},w={tile_shape[0]},h={tile_shape[1]}].jpg"
            save_path = os.path.join(output_dir, file_name)
            cv2.imwrite(save_path,
                        img[y_pos - offset:y_pos + tile_height + offset, x_pos - offset:x_pos + tile_width + offset])


def image_from_tiles(input_dir: str, output_name: str, shape: tuple, tile_ending: str, pat_id: str, overlap: int = 0, tile_dims: tuple=(0,0), scaling=1.0):
    """
    Function to recreate an image from previously made tiles. Requires a folder with
    the tiles, the ending of images that are tiles (to make sure only tile are used),
    output name of the image and overlap of the tile (if none leave out)

    :param shape: shape of tiles (eg. (512,512))
    :param input_dir: directory with the image tiles
    :param output_name: name of the recreated image
    :param tile_ending: Ending of the filename of tiles (eg. .png) that only the tiles have
    :param overlap: overlap of the tiles (total amount of overlap, left overlap + right overlap)
    :param tile_dims
    :param scaling: scale of which the output image will be set to. Must be smaller equal one
    :return: dimension of image in tiles
    """
    if overlap % 2 != 0:
        print('Error, overlap must be dividable by 2 as overlap/2 is added around the corners')
        return None

    tile_names = [tile_name for tile_name in os.listdir(input_dir) if tile_name.endswith(f"{tile_ending}")]


    print("setting tile_dims automatically")
    # Find max x and max y
    x_cords = list()
    y_cords = list()
    for tile_name in tile_names:
        start_x = tile_name.find('x=') + 2
        end_x = tile_name.find(',', start_x)
        start_y = tile_name.find('y=') + 2
        end_y = tile_name.find(',', start_y)
        x_cords.append(int(tile_name[start_x:end_x]))
        y_cords.append(int(tile_name[start_y:end_y]))
    y_cords.sort()
    x_cords.sort()
    if tile_dims == (0, 0):
        row_length = int((x_cords[-1] - x_cords[0])/shape[0]) + 1
        column_length = int((y_cords[-1] - y_cords[0])/shape[1]) + 1
    else:
        row_length = tile_dims[0]
        column_length = tile_dims[1]
    print(f"New image will have dimensions: {int(row_length*(shape[0]-(overlap/2)))}x{int(column_length*(shape[1]-(overlap/2)))}px, using {int(row_length*column_length)} tiles (of {len(tile_names)})")

    # Generate column images
    offset = int(overlap / 2)
    width = shape[0] - overlap
    height = shape[1] - overlap
    # columns =[]
    rec_img = np.ones((column_length * width, row_length * width, 3), dtype=np.uint8)
    rec_img.fill(255)
    missing = 0
    total = 0
    for column in tqdm(range(column_length)):
        for row in range(row_length):
            tile_name = input_dir + pat_id +" [x=" + str(x_cords[0] + row * width)+",y=" + str(y_cords[0] + column * height)+ ",w=396,h=396]"+ tile_ending
            img = cv2.imread(tile_name)
            if img is not None:
                y = column * height
                x = row * width
                try:
                    # Replace classes by values
                    img[np.where((img == [0, 0, 0]).all(axis=2))] = [0, 255, 0]
                    img[np.where((img == [1, 1, 1]).all(axis=2))] = [255, 0, 0]
                    img[np.where((img == [2, 2, 2]).all(axis=2))] = [0, 0, 255]
                    # Place tile into bigger image
                    rec_img[y:y + height, x:x + width] = img[offset:height + offset, offset:width + offset]
                    total += 1
                except Exception as exception:
                    print(f"Error at x={x},y={y}, {exception}")
                    return
            else:
                missing += 1
                total += 1
                #print(f"Error missing img: {tile_name}")

    # Concat the columns in order to get the final image
    print(f"Total missing: {missing}/{total}")
    print(cv2.imwrite(output_name, rec_img))


def image_predicted_from_tiles(input_dir_tile: str,
                                input_dir_prediction: str,
                                output_dir: str,
                                shape: tuple,
                                shape_tile:tuple,
                                tile_ending: str,
                                pat_id: str,
                                overlap: int = 0,
                                overlap_tile =0,
                                scaling=1.0):
    """
    Function to recreate an image from previously made tiles. Requires a folder with
    the tiles, the ending of images that are tiles (to make sure only tile are used),
    output name of the image and overlap of the tile (if none leave out)

    :param shape: shape of tiles (eg. (512,512))
    :param input_dir_image: directory with the image tiles
    :param input_dir_masks: directpry for masks
    :param output_name: name of the recreated image
    :param tile_ending: Ending of the filename of tiles (eg. .png) that only the tiles have
    :param overlap: overlap of the tiles (total amount of overlap, left overlap + right overlap)
    :param scaling: scale of which the output image will be set to. Must be smaller equal one
    :return:
    """
    if overlap % 2 != 0:
        print('Error, overlap must be dividable by 2 as overlap/2 is added around the corners')
        return

    mask_names = [tile_name for tile_name in os.listdir(input_dir_prediction) if tile_name.endswith(f"{tile_ending}")]
    # Find max x and max y
    x_coords = list()
    y_coords = list()
    for mask_name in mask_names:
        start_x = mask_name.find('x=') + 2
        end_x = mask_name.find(',', start_x)
        start_y = mask_name.find('y=') + 2
        end_y = mask_name.find(',', start_y)
        x_coords.append(int(mask_name[start_x:end_x]))
        y_coords.append(int(mask_name[start_y:end_y]))
    y_coords.sort()
    x_coords.sort()
    row_length = int((x_coords[-1] - x_coords[0])/shape[0]) + 1
    column_length = int((y_coords[-1] - y_coords[0]) / shape[1]) + 1
    # print(f"amount rows: {row_length}, amount columns: {column_length}")

    # Generate column images
    offset_tile = int(overlap_tile / 2)
    width_tile = shape_tile[0] - overlap_tile
    height_tile = shape_tile[1] - overlap_tile



    offset = int(overlap / 2)
    width = shape[0] - overlap
    height = shape[1] - overlap
    # columns =[]
    rec_pred = np.ones((column_length * width, row_length * width, 3), dtype=np.uint8)
    rec_pred.fill(255)

    rec_original = np.ones((column_length * width, row_length * width, 3), dtype=np.uint8)
    rec_original.fill(255)

    missing = 0
    total = 0
    for column in tqdm(range(column_length)):
        for row in range(row_length):
            tile_path = input_dir_tile + '\\' +pat_id + " [x=" + str(x_coords[0] + row * width_tile)+",y=" + str(y_coords[0] + column * height_tile) + ",w=396,h=396]" + tile_ending
            predicted_path = input_dir_prediction + '\\' +pat_id + " [x=" + str(x_coords[0] + row * width) + ",y=" + str(y_coords[0] + column * height) + ",w=396,h=396]" + tile_ending

            original = cv2.imread(tile_path)
            predicted = cv2.imread(predicted_path)

            if predicted is not None:
                y = column * height
                x = row * width
                try:
                    # Replace classes by values
                    stroma = [0, 255, 0]
                    tumor = [255, 0, 0]
                    immune_cells = [0, 0, 255]

                    predicted[np.where((predicted == [0, 0, 0]).all(axis=2))] = stroma
                    predicted[np.where((predicted == [1, 1, 1]).all(axis=2))] = immune_cells
                    predicted[np.where((predicted == [2, 2, 2]).all(axis=2))] = tumor

                    # Place tile into bigger image
                    rec_original[y:y + height, x:x + width] = original[offset_tile:height_tile + offset_tile, offset_tile:width_tile + offset_tile]
                    rec_pred[y:y + height, x:x + width] = predicted[offset:height + offset, offset:width + offset]
                    total += 1
                except Exception as exception:
                    print(f"Error at x={x},y={y}, {exception}")
                    return
            else:
                missing += 1
                total += 1

    # Concat the columns in order to get the final image
    print(cv2.imwrite(os.path.join(output_dir, f'{pat_id}_original.png'), rec_original))
    print(cv2.imwrite(os.path.join(output_dir, f'{pat_id}_predicted.png'), rec_pred))

    print(f"{missing}/{total} used")


def image_mask_predicted_from_tiles(input_dir_tile: str,
                                    input_dir_prediction: str,
                                    input_dir_masks: str,
                                    output_dir: str,
                                    shape: tuple,
                                    shape_tile:tuple,
                                    tile_ending: str,
                                    mask_ending: str,
                                    pat_id: str,
                                    overlap: int = 0,
                                    overlap_tile =0,
                                    scaling=1.0):
    """
    Function to recreate an image from previously made tiles. Requires a folder with
    the tiles, the ending of images that are tiles (to make sure only tile are used),
    output name of the image and overlap of the tile (if none leave out)

    :param shape: shape of tiles (eg. (512,512))
    :param input_dir_image: directory with the image tiles
    :param input_dir_masks: directpry for masks
    :param output_name: name of the recreated image
    :param tile_ending: Ending of the filename of tiles (eg. .png) that only the tiles have
    :param overlap: overlap of the tiles (total amount of overlap, left overlap + right overlap)
    :param scaling: scale of which the output image will be set to. Must be smaller equal one
    :return:
    """
    if overlap % 2 != 0:
        print('Error, overlap must be dividable by 2 as overlap/2 is added around the corners')
        return

    mask_names = [tile_name for tile_name in os.listdir(input_dir_masks) if tile_name.endswith(f"{mask_ending}")]

    # name of img (base before brackets)
    pat_name = mask_names[0][:mask_names[0].rfind('[')]
    # Find max x and max y
    x_coords = list()
    y_coords = list()
    for mask_name in mask_names:
        start_x = mask_name.find('x=') + 2
        end_x = mask_name.find(',', start_x)
        start_y = mask_name.find('y=') + 2
        end_y = mask_name.find(',', start_y)
        x_coords.append(int(mask_name[start_x:end_x]))
        y_coords.append(int(mask_name[start_y:end_y]))
    y_coords.sort()
    x_coords.sort()
    row_length = int((x_coords[-1] - x_coords[0])/shape[0]) + 1
    column_length = int((y_coords[-1] - y_coords[0]) / shape[1]) + 1
    # print(f"amount rows: {row_length}, amount columns: {column_length}")

    # Generate column images
    offset_tile = int(overlap_tile / 2)
    width_tile = shape_tile[0] - overlap_tile
    height_tile = shape_tile[1] - overlap_tile



    offset = int(overlap / 2)
    width = shape[0] - overlap
    height = shape[1] - overlap
    # columns =[]
    rec_pred = np.ones((column_length * width, row_length * width, 3), dtype=np.uint8)
    rec_pred.fill(255)

    rec_mask = np.ones((column_length * width, row_length * width, 3), dtype=np.uint8)
    rec_mask.fill(255)

    rec_original = np.ones((column_length * width, row_length * width, 3), dtype=np.uint8)
    rec_original.fill(255)

    missing = 0
    total = 0
    for column in tqdm(range(column_length)):
        for row in range(row_length):
            tile_path = input_dir_tile + pat_id + " [x=" + str(x_coords[0] + row * width_tile)+",y=" + str(y_coords[0] + column * height_tile) + ",w=396,h=396]" + tile_ending
            mask_path = input_dir_masks + pat_id + " [x=" + str(x_coords[0] + row * width)+",y=" + str(y_coords[0] + column * height) + ",w=396,h=396]" + mask_ending
            predicted_path = input_dir_prediction + pat_id + " [x=" + str(x_coords[0] + row * width) + ",y=" + str(y_coords[0] + column * height) + ",w=396,h=396]" + tile_ending

            original = cv2.imread(tile_path)
            mask = cv2.imread(mask_path)
            predicted = cv2.imread(predicted_path)

            if mask is not None:
                y = column * height
                x = row * width
                try:
                    # Replace classes by values
                    stroma = [0, 255, 0]
                    tumor = [255, 0, 0]
                    immune_cells = [0, 0, 255]

                    mask[np.where((mask == [0, 0, 0]).all(axis=2))] = stroma
                    mask[np.where((mask == [1, 1, 1]).all(axis=2))] = immune_cells
                    mask[np.where((mask == [2, 2, 2]).all(axis=2))] = tumor

                    predicted[np.where((predicted == [0, 0, 0]).all(axis=2))] = stroma
                    predicted[np.where((predicted == [1, 1, 1]).all(axis=2))] = immune_cells
                    predicted[np.where((predicted == [2, 2, 2]).all(axis=2))] = tumor

                    # Place tile into bigger image
                    rec_original[y:y + height, x:x + width] = original[offset_tile:height_tile + offset_tile, offset_tile:width_tile + offset_tile]
                    rec_mask[y:y + height, x:x + width] = mask[offset:height + offset, offset:width + offset]
                    rec_pred[y:y + height, x:x + width] = predicted[offset:height + offset, offset:width + offset]
                    total += 1
                except Exception as exception:
                    print(f"Error at x={x},y={y}, {exception}")
                    return
            else:
                missing += 1
                total += 1
                #print(f"Error missing img: {tile_path}")
                #return # Do not return, missing places stay white

    # Concat the columns in order to get the final image
    print(cv2.imwrite(os.path.join(output_dir, f'{pat_id}_original.png'), rec_original))
    print(cv2.imwrite(os.path.join(output_dir, f'{pat_id}_mask.png'), rec_mask))
    print(cv2.imwrite(os.path.join(output_dir, f'{pat_id}_predicted.png'), rec_pred))

    print(f"{missing}/{total} used")


if __name__ == '__main__':
    dims = (256, 256)
    image_from_tiles('L:\\PDAC-TSR-Xiuxiang\\AC\\masks\\10-18836.tif - Series 0\\',
                     'C:\\Users\\Working_Group_Lara\\Documents\\unet\\10-18836.png',
                      (212, 212),
                     '-mask.png')
