"""Module to prepare data for processing"""

import os
import numpy as np
import shutil
from keras.preprocessing import image
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import cv2
from src.config import output_height, output_width


def copy_tiles(path_normalized, path_labeled, path_output) -> None:
    """
    Takes paths of Normalized tiles and labeled images and copies them into the corresponding data directory.
    If cohort is empty the cohort is derived from the tile names. Paths should contain subfolders with tiles
    inside.
    :param path_normalized: path to folder that contains subfolders with tiles that are normalized
    :param path_labeled: path to folder that contains subfolders with tiles that are labelled
    :param path_output: path to copy data to
    :return:
    """
    if not (os.path.exists(path_normalized) or os.path.exists(path_labeled) or os.path.exists(path_output)):
        raise Exception("Error one of the given paths is invalid")
    norm_pat = os.listdir(path_normalized)
    labl_pat = os.listdir(path_labeled)

    # For each patient folder just copy images to given destination (Cohort)
    for pat in tqdm(norm_pat):
        path_to_pat_dir = os.path.join(path_normalized, pat)
        for tile in [tile for tile in os.listdir(path_to_pat_dir) if tile.endswith('].jpg')]:
            shutil.copyfile(os.path.join(path_to_pat_dir, tile), os.path.join(path_output, tile))
    # Copy labelled images
    for pat in tqdm(labl_pat):
        path_to_pat_dir = os.path.join(path_labeled, pat)
        for tile in [tile_labelled for tile_labelled in os.listdir(path_to_pat_dir) if
                     tile_labelled.endswith('labelled.png')]:
            shutil.copyfile(os.path.join(path_to_pat_dir, tile), os.path.join(path_output, tile))


def make_list(test_size, cohort_path: str = "../../data/TCGA"):
    """
    Writes all filenames from files of the 'tiles' directory into the files test.txt and trainval.txt
    :param cohort_path: path to the directory of the Cohort
    :param test_size: Percentage or amount of tiles that are used for testing
    :return:
    """
    # Get Path to tiles
    tile_path = os.path.join(cohort_path, 'tiles')
    # Extract all files that are jpgs and delete the file ending in the filename
    files = [file[:-4] for file in os.listdir(tile_path) if file.endswith(".jpg")]
    trainval_list, test_list = train_test_split(files, test_size=test_size)
    missing_tv = validate_list(trainval_list, tile_path)
    missing_t = validate_list(test_list, tile_path)

    if missing_tv:
        for name in missing_tv:
            print(f"{name} is missing and therefore removed from the dataset list")
            trainval_list.remove(name)
    if missing_t:
        for name in missing_t:
            print(f"{name} is missing and therefore removed from the dataset list")
            test_list.remove(name)
    # Write file names to corresponding lists
    with open(os.path.join(cohort_path, 'test.txt'), "w+") as file:
        for filename in test_list:
            file.write(filename + "\n")
    with open(os.path.join(cohort_path, 'trainval.txt'), "w+") as file:
        for filename in trainval_list:
            file.write(filename + "\n")


def validate_list(tile_list, tile_path: str) -> list:
    """
    Returns a list of tiles, where the tiles have one missing part (either original image or the mask)
    :param tile_list: list of all tile base names (without ending and without label)
    :param tile_path: directory with the tiles being used
    :return:
    """
    existing = os.listdir(tile_path)
    missing = []
    for tile_name in tile_list:
        if f"{tile_name}-labelled.png" in existing and f"{tile_name}.jpg" in existing:
            pass
        else:
            missing += [tile_name]
            print(f"{tile_name} is missing")
    return missing


def extract_class_tiles(tile_path, extracted_tile_path):
    """
    Copys all tiles from tilepath to a directory, where all tiles being copied have at least one class that is unequal
    to the 0(=background) class

    :param tile_path: path to tile directory
    :param extracted_tile_path: path to output directory
    :return:
    """
    labeled_tiles = [lb_tile for lb_tile in os.listdir(tile_path) if lb_tile.endswith("png")]
    unique_values = set()

    for i in tqdm(range(len(labeled_tiles)), desc='Processing images'):
        fp = os.path.join(tile_path, labeled_tiles[i])
        img = image.load_img(fp)
        img = image.img_to_array(img)
        # unique_values.update(set(np.unique(img)))
        contains = set(np.unique(img))
        # If any non 0 pixels are in the image
        if 128.0 in contains or 200.0 in contains or 150.0 in contains:
            fp_dst = os.path.join(extracted_tile_path, labeled_tiles[i])
            # Copy original
            shutil.copyfile(f"{fp[:-13]}.jpg", f"{fp_dst[:-13]}.jpg")
            # Copy mask
            shutil.copyfile(fp, fp_dst)


def get_class_distribution(tile_path: str):
    class_dist = {"Tumor": 0, "Stroma": 0, "Immune Cells": 0, "Background": 0, "Total": 0}
    labeled_tiles = [lb_tile for lb_tile in os.listdir(tile_path) if lb_tile.endswith("png")]
    class_dist["Total"] = len(labeled_tiles)

    for i in tqdm(range(len(labeled_tiles)), desc='Processing images'):
        fp = os.path.join(tile_path, labeled_tiles[i])
        img = cv2.imread(fp, cv2.IMREAD_COLOR)

        contains = np.unique(img.reshape(-1, img.shape[2]), axis=0)
        if [255, 255, 255] in contains:
            class_dist["Background"] += 1
        if [128, 128, 0] in contains:
            class_dist["Immune Cells"] += 1
        if [200, 0, 0] in contains:
            class_dist["Tumor"] += 1
        if [150, 200, 150] in contains:
            class_dist["Stroma"] += 1
    print(class_dist)


def generate_mask(tile_path):
    """
    Generates a multiclass mask out of labelled tiles that were extracted from Qupath
    :param tile_path: path to the labelled tiles
    :return:
    """
    labeled_tiles = [lb_tile for lb_tile in os.listdir(tile_path) if lb_tile.endswith("labelled.png")]
    existing = [file for file in os.listdir(tile_path) if file.endswith("mask.png")]
    for i in tqdm(range(len(labeled_tiles)), desc='Processing images'):
        # Skipping all already masked images
        if f"{labeled_tiles[i][:-13]}-mask.png" in existing:
            continue

        fp = os.path.join(tile_path, labeled_tiles[i])
        img = cv2.imread(fp, cv2.IMREAD_COLOR)
        height = img.shape[0]
        width = img.shape[1]
        offset_y = int((height-output_height)/2)
        offset_x = int((width-output_width)/2)
        new_mask = np.zeros((output_width, output_height, 1), np.uint8)
        rows, cols, dr = (output_height, output_width, 3)

        class_value = 0
        for r in range(rows):
            for c in range(cols):
                val = img[offset_y+r, offset_x+c]
                if all(val == [255, 255, 255]):
                    # Background
                    class_value = 0
                elif all(val == [128, 128, 0]) or all(val == [0, 128, 128]) or all(val == [160, 90, 160]):
                    # Immune Cells (Either dark green or deep purple)
                    class_value = 3
                elif all(val == [200, 0, 0]) or all(val == [0, 0, 200]):
                    # Tumor
                    class_value = 2
                elif all(val == [150, 200, 150]):
                    # Stroma
                    class_value = 1
                else:
                    raise Exception(f"Error unexpexcted pixel values: {val} at {r}, {c}")
                new_mask[r, c] = class_value
        cv2.imwrite(f"{fp[:-13]}-mask.png", new_mask)


if __name__ == '__main__':
    make_list(5)
    # extract_class_tiles('J:/tiles/TCGA-2J-AAB1-01Z-00-DX1.F3B4818F-9C3B-4C66-8241-0570B2873EC9', 'J:/tiles/test_filtered')
    # {0.0, 128.0, 200.0, 150.0, 255.0}
    # get_class_distribution('J:/tiles/test_filtered')
    # generate_mask()
