"""Module to generate Set lists (tiles, excluded, eg.) for cohorts."""
import os
from sklearn.model_selection import train_test_split
import pandas as pd
from src.config import tiles_header
from src.util.cohort import Cohort
from tqdm import tqdm
import cv2 as cv
import numpy as np


def get_usable_tiles(cohort_path: str) -> list:
    """
    Returns the names of all tiles that have both mask and labelled version and are suitable for training
    (Includes the manually confirmed tiles).
    :param cohort_path: path to the directory of the Cohort
    :return:
    """
    df_tiles = pd.read_csv(os.path.join(cohort_path, 'tiles.csv'))
    usable = list(df_tiles[df_tiles['excluded'] == False]['Tile'].values)
    return usable


def make_list(cohort: Cohort) -> None:
    """
    Writes all file names from files of the 'tiles' directory into the file tiles.csv and validates them (tiles with
    missing labelled version or missing mask will be marked as excluded. It will also override any existing tiles.csv
    file.
    :param cohort: Cohort object of the current cohort
    :return:
    """
    # Detect if tiles are contained in nested dirs (with at least 2 Patients)
    dirs = [directory for directory in os.listdir(cohort.tiles_path) if os.path.isdir(os.path.join(cohort.tiles_path, directory))]
    # Only take is 2 patients are there, in case there is a single unrelated dir in the folder with the tiles
    if len(dirs) >= 2:
        files = []
        for directory in dirs:
            files += [os.path.join(directory, file[:-4]) for file in os.listdir(os.path.join(cohort.tiles_path, directory)) if file.endswith(".jpg")]
    else:
        files = [file[:-4] for file in os.listdir(cohort.tiles_path) if file.endswith(".jpg")]

    # Create new df
    df_tiles = pd.DataFrame({'Tile': files})
    for col in tiles_header:
        if col == 'valid' or col == 'excluded' or col == 'potentiallyBad' or col == 'updated':
            df_tiles[col] = False
        else:
            df_tiles[col] = ''

    df_tiles.to_csv(os.path.join(cohort.trainval_path, 'tiles.csv'), index=False)
    validate_list(cohort, False)
    write_suspected_bad_tiles(cohort)


def make_train_val(cohort: Cohort, test_size) -> None:
    """
        Generates the train/validation/test set and saves training/validation tiles in trainval.txt and
        test tiles in test.txt.
        :param cohort: Cohort being used
        :param test_size: Percentage or amount of tiles that are used for testing
        :return:
        """
    tiles = get_usable_tiles(cohort.trainval_path)

    trainval_list, test_list = train_test_split(tiles, test_size=test_size)
    # Write file names to corresponding lists
    with open(os.path.join(cohort.trainval_path, 'test.txt'), "w+") as file:
        for filename in test_list:
            file.write(filename + "\n")
    with open(os.path.join(cohort.trainval_path, 'trainval.txt'), "w+") as file:
        for filename in trainval_list:
            file.write(filename + "\n")


def validate_list(cohort: Cohort, check_mask: bool) -> None:
    """
    Checks all tiles in tiles.csv if they have a mask and a labelled version, if not they are marked as excluded
    in tiles.csv
    :param check_mask: if true will also check if masks exist
    :param cohort: cohort being used
    :return:
    """
    has_mask = []
    has_labl = []

    df_tiles = pd.read_csv(os.path.join(cohort.trainval_path, 'tiles.csv'))
    tiles = list(df_tiles['Tile'])
    for tile in tiles:
        if os.path.isfile(os.path.join(cohort.lbld_tiles_path, tile+'-labelled.png')):
            has_labl += [tile]
        if check_mask and os.path.isfile(os.path.join(cohort.mask_path, tile+'-mask.png')):
            has_mask += [tile]

    if check_mask:
        valid = list(set(has_labl).intersection(set(has_mask)))
    else:
        valid = list(set(has_labl))
    df_tiles.set_index('Tile', inplace=True)
    # Set all to excluded (valid ones will be included again)
    df_tiles['excluded'] = True
    # Set valid ones in dataframe
    for valid_tile in valid:
        df_tiles.at[valid_tile, 'valid'] = True
        df_tiles.at[valid_tile, 'excluded'] = False
    df_tiles.to_csv(os.path.join(cohort.trainval_path, 'tiles.csv'))
    print(f"{len(set(tiles)-set(valid))} tiles have been excluded due to missing labels or missing masks of them")


def write_suspected_bad_tiles(cohort: Cohort) -> None:
    """
    Marks all tiles that could potentially be bad/unusable as potentionallyBad in tiles.csv. If a tile is marked as
    potentionallyBad and has no value in revised, it is also marked as excluded
    :param cohort: Cohort being used
    :return:
    """
    tile_file_path = os.path.join(cohort.trainval_path, 'tiles.csv')
    df_tiles = pd.read_csv(tile_file_path)
    tiles = list(df_tiles['Tile'].values)
    sus_fill = []
    sus_bg = []

    # All tiles that contain background might be bad aswell as all tile that contain only stroma
    for tile in tqdm(tiles):
        lbld_tile = cv.imread(os.path.join(cohort.lbld_tiles_path, tile + '-labelled.png'), cv.IMREAD_GRAYSCALE)
        pix_vals = np.unique(lbld_tile)
        # 118 = Immune cells, 179 = Stroma, 255 = BG, 59 = Tumor
        if len(pix_vals) == 1 and (179 in pix_vals or 255 in pix_vals):
            sus_bg += [tile]
        #Mixed colors in tile
        elif 255 in pix_vals:
            sus_fill += [tile]
    df_tiles.set_index('Tile', inplace=True)

    for tile in set(sus_bg).union(set(sus_fill)):
        df_tiles.at[tile, 'potentiallyBad'] = True
        df_tiles.at[tile, 'excluded'] = True

    df_tiles.to_csv(tile_file_path)
    print(f"{len(set(sus_bg).union(set(sus_fill)))} tiles have been excluded due to being potentially Bad")


def make_tile_txt_list(tiles_path: str) -> None:
    tiles = [tile[:-4] for tile in os.listdir(tiles_path) if tile.endswith('.jpg')]
    with open(os.path.join(tiles_path, 'tiles.txt'), "w+") as file:
        for filename in tiles:
            file.write(filename + "\n")


def get_tiles_stats(cohort: Cohort) -> None:
    df_tiles = pd.read_csv(os.path.join(cohort.trainval_path, 'tiles.csv'))
    print(f"Total tiles: {len(df_tiles)}")
    print(f"Tiles used: {len(list(df_tiles[df_tiles['excluded']==False]['Tile'].values))}")
    print(f"Total excluded:{len(list(df_tiles[df_tiles['excluded']==True]['Tile'].values))}")
    print(f"Total potentially Bad:{len(list(df_tiles[df_tiles['potentiallyBad']==True]['Tile'].values))}")
    print(f"Total revised:{len(list(df_tiles.dropna(subset=['revision'])['Tile'].values))}")
    df_tiles = df_tiles.dropna(subset=['revision'])
    print(f"Revised and excluded:{len(list(df_tiles[df_tiles['revision'].isin(['excluded'])]['Tile'].values))}")
    print(f"Revised, accepted or filled and updated:{len(list(df_tiles[df_tiles['updated']==True & df_tiles['revision'].isin(['accepted', 'fill'])]['Tile'].values))}")





if __name__ == '__main__':
    make_tile_txt_list('X:\\Projects\\Classifier\\data\\DU\\tiles_nn')
